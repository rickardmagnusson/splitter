
/*
* Splitter component
* Creates a resizable divs with a handle between two objects. 
*/
class Splitter {
    /*The component to init as container for the splitter*/
    constructor(component, window) {
        this.component = component;
        this.window = window;
        this.left = component.querySelector('[data-view]')[0];
        this.handle = component.querySelector('[data-handle]');
        this.handle.title = "Drag to resize columns.";
        this.initials = this.window.getComputedStyle(this.component).getPropertyValue('--grid-template-columns');
        this.defaultLayout = this.initials; //Store default

        //Web
        this.handle.addEventListener('mousedown', this.startDrag.bind(this), false);
        this.component.addEventListener('mousemove', this.onDrag.bind(this), false);
        this.component.addEventListener('mouseup', this.endDrag.bind(this), false);
        this.window.addEventListener("resize", this.onResize.bind(this), true);

        //Touch screens such as tablets, phones etc..
        this.handle.addEventListener("touchstart", this.startDrag.bind(this), false);
        this.component.addEventListener("touchend", this.endDrag.bind(this), false);
        this.component.addEventListener("touchmove", this.onDrag.bind(this), false);

        this.isDragging = false;
    }

    /*
        Reset layout on resize */
    onResize(){
        this.component.style.setProperty('--grid-template-columns', this.defaultLayout);
    }

    /*
        Prevent drag event while not active */
    startDrag(){ 
        this.isDragging = true; 
    }
    
    /*
        Remove drag. Clean up event */
    endDrag(){ 
        this.isDragging = false; 
    }

    onDrag(e){ 
        if(this.isDragging){
            let colWidth = e.type == 'touchstart' ? e.touches[0].clientX : e.clientX;
            /*left|handle|Rest of the width*/
            let cols = [
                colWidth, 
                this.handle.clientWidth, 
                this.component.clientWidth - this.handle.clientWidth  - colWidth
            ];
            let newColDefn = cols.map(c => c.toString() + "px").join(" ");
            this.component.style.setProperty('--grid-template-columns', newColDefn);
            e.preventDefault()
        }
    }
}

const splitcomponents = document.querySelectorAll('[data-split]');
splitcomponents.forEach(splitElement => new Splitter(splitElement, window));