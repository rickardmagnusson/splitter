# Javscript Splitter (Pure Javascript component) #

![img/splitter.png](img/splitter.png)

### Split's two div's into resizable areas ###
    Uses grid-layout:
     grid-template-columns: var(--grid-template-columns); 
     where --grid-template-columns = 2fr 6px 3fr


### Setup the component ###

    * Add [data-split] to the component you like to add a splitter